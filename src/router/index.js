import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import List from '@/components/List'
import Edit from '@/components/Edit'
import Arianism from '@/components/Arianism'
import Docetism from '@/components/Docetism'
import Monarchianism from '@/components/Monarchianism'
import Monothelitism from '@/components/Monothelitism'
import Nestorianism from '@/components/Nestorianism'
import Sabellianism from '@/components/Sabellianism'
import Tritheism from '@/components/Tritheism'
import Catharism from '@/components/Catharism'
import Valentianism from '@/components/Valentianism'
import Manichaeism from '@/components/Manichaeism'
import Luciferians from '@/components/Luciferians'
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/arianism',
      name: 'arianism',
      component: Arianism
    },
    {
      path: '/docetism',
      name: 'docetism',
      component: Docetism
    },
    {
      path: '/valentianism',
      name: 'valentianism',
      component: Valentianism
    },
    {
      path: '/manichaeism',
      name: 'manichaeism',
      component: Manichaeism
    },
    {
      path: '/luciferians',
      name: 'luciferians',
      component: Luciferians
    },
    {
      path: '/monarchianism',
      name: 'monarchianism',
      component: Monarchianism
    },
    {
      path: '/catharism',
      name: 'catharism',
      component: Catharism
    },
    {
      path: '/tritheism',
      name: 'tritheism',
      component: Tritheism
    },
    {
      path: '/sabellianism',
      name: 'sabellianism',
      component: Sabellianism
    },
    {
      path: '/nestorianism',
      name: 'nestorianism',
      component: Nestorianism
    },
    {
      path: '/monothelitism',
      name: 'monothelitism',
      component: Monothelitism
    },
    {
      path: '/monarchianism',
      name: 'monarchianism',
      component: Monarchianism
    },
    {
      path: '/forms',
      name: 'list',
      component: List
    },
    {
      path: '/edit/:id',
      name: 'edit',
      component: Edit
    }
  ]
})
